package com.tedmetzger.hearstreader.app.core;


/**
 * Created by reneravenel on 9/2/14.
 */
public class ViafouraJsonArticle {

    Response responses;

    public class Response {
        First first;
    }

    public class First {
        String http_status;
        Result result;
    }

    public class Result {
        String num_replies;
        String title;
        String date_created;
        String id;
        String image_url;
    }

    public int getCommentCount() {
        try {
            return Integer.parseInt(responses.first.result.num_replies);
        }
        catch (NumberFormatException e) {
            return 0;
        }
    }

    public String getTitle() {
        return responses.first.result.title;
    }

    public int getTimestamp() {
        try {
            return Integer.parseInt(responses.first.result.date_created);
        }
        catch (NumberFormatException e) {
            return 0;
        }
    }

    public String getId() {
        return responses.first.result.id;
    }

    public String getImageUrl() {
        return responses.first.result.image_url;
    }
}
