package com.tedmetzger.hearstreader.app.core;

/**
 * Created by reneravenel on 9/2/14.
 */
public enum FeedSort {

    velocity,
    latest,
    comment;

}
