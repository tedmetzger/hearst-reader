package com.tedmetzger.hearstreader.app.task;

/**
 * Created by jamesmetzger on 8/12/14.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tedmetzger.hearstreader.app.core.Channel;
import com.tedmetzger.hearstreader.app.manager.FeedManager;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetRssFeedTask extends AsyncTask<Void, Void, List<String>> {

    private static final String XML_RSS = "rss";
    private static final String XML_CHANNEL = "channel";
    private static final String XML_TITLE = "title";
    private static final String XML_LINK = "link";
    private static final String XML_ITEM = "item";

    Context ctx;
    Channel channel;

    public GetRssFeedTask(Context ctx, Channel channel){
        this.ctx = ctx;
        this.channel = channel;
    }

    @Override
    protected List<String> doInBackground(Void... voids) {
        List<String> result = null;
        try {
            String feed = getRssFeed();
            result = parse(feed);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getRssFeed() throws IOException {
        InputStream in = null;
        String rssFeed = null;
        try {
            URL url = new URL(this.channel.getUrl());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            in = conn.getInputStream();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            for (int count; (count = in.read(buffer)) != -1; ) {
                out.write(buffer, 0, count);
            }
            byte[] response = out.toByteArray();
            rssFeed = new String(response, "UTF-8");
        } finally {
            if (in != null) {
                in.close();

            }
        }
        return rssFeed;
    }

    private List<String> parse(String rssFeed) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new StringReader(rssFeed));
        xpp.nextTag();
        return readRss(xpp);
    }

    private List<String> readRss(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        List<String> items = new ArrayList<String>();
        parser.require(XmlPullParser.START_TAG, null, XML_RSS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(XML_CHANNEL)) {
                items.addAll(readChannel(parser));
            } else {
                skip(parser);
            }
        }
        return items;
    }

    private List<String> readChannel(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<String> items = new ArrayList<String>();
        parser.require(XmlPullParser.START_TAG, null, XML_CHANNEL);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(XML_ITEM)) {
                items.add(readItem(parser));
            } else {
                skip(parser);
            }
        }
        return items;
    }

    private String readItem(XmlPullParser parser) throws XmlPullParserException, IOException {
        String result = null;
        parser.require(XmlPullParser.START_TAG, null, XML_ITEM);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(XML_TITLE)) {
                result = readTitle(parser);
            }
            else if (name.equals(XML_LINK)) {
                String link = readLink(parser);

                FeedManager.INSTANCE.incrementArticleTaskCount();
                new GetArticleTask(ctx, link, channel).execute();
            }
            else {
                skip(parser);
            }
        }
        return result;
    }

    // Processes title tags in the feed.
    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, XML_TITLE);
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, XML_TITLE);
        return title;
    }

    private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, XML_LINK);
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, XML_LINK);
        return title;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
