package com.tedmetzger.hearstreader.app.manager;

import android.graphics.Bitmap;

import java.util.HashMap;

/**
 * Created by jamesmetzger on 8/13/14.
 */
public class ArticleImageManager {

    private HashMap<String,Bitmap> urlsToImages = new HashMap<String, Bitmap>();

    public static ArticleImageManager INSTANCE = new ArticleImageManager();

    private ArticleImageManager() {}

    public Bitmap getBitmapForUrl(String url) {
        return urlsToImages.get(url);
    }

    public void setBitmapForUrl(String url, Bitmap bitmap) {
         urlsToImages.put(url,bitmap);
    }
}
