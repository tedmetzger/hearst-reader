package com.tedmetzger.hearstreader.app.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.tedmetzger.hearstreader.app.core.Article;

/**
 * Created by reneravenel on 8/28/14.
 */
public class DbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "HearstReader.db";

    public static final String TEXT_TYPE = " TEXT";
    public static final String COMMA_SEP = ",";


    //-- API methods --//

    public Article createArticle(Article article) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DbContract.DbArticle.COLUMN_NAME_FEED_ID , article.getFeedId());
        values.put(DbContract.DbArticle.COLUMN_NAME_TITLE , article.getTitle());
        values.put(DbContract.DbArticle.COLUMN_NAME_COMMENT_COUNT , article.getCommentCount());
        values.put(DbContract.DbArticle.COLUMN_NAME_URL , article.getUrl());
        values.put(DbContract.DbArticle.COLUMN_NAME_IMAGE_URL , article.getImageUrl());
        values.put(DbContract.DbArticle.COLUMN_NAME_IMAGE_FILE , article.getImageFile());
        values.put(DbContract.DbArticle.COLUMN_NAME_ARTICLE_FILE , article.getArticleFile());
        values.put(DbContract.DbArticle.COLUMN_NAME_STATE , article.getStateBitMask());
        values.put(DbContract.DbArticle.COLUMN_NAME_TIMESTAMP , article.getTimestamp());

        long newRowId = db.insert(
                DbContract.DbArticle.TABLE_NAME,
                null,
                values);

        article.setDbId(newRowId);

        return article;

    }

    //-- Lifecycle management --//

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DbContract.DbArticle.TABLE_NAME + " (" +
                    DbContract.DbArticle._ID + " INTEGER PRIMARY KEY," +

                    DbContract.DbArticle.COLUMN_NAME_FEED_ID + TEXT_TYPE + COMMA_SEP +
                    DbContract.DbArticle.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    DbContract.DbArticle.COLUMN_NAME_COMMENT_COUNT + TEXT_TYPE + COMMA_SEP +
                    DbContract.DbArticle.COLUMN_NAME_URL + TEXT_TYPE + COMMA_SEP +
                    DbContract.DbArticle.COLUMN_NAME_IMAGE_URL + TEXT_TYPE + COMMA_SEP +
                    DbContract.DbArticle.COLUMN_NAME_IMAGE_FILE + TEXT_TYPE + COMMA_SEP +
                    DbContract.DbArticle.COLUMN_NAME_ARTICLE_FILE + TEXT_TYPE + COMMA_SEP +
                    DbContract.DbArticle.COLUMN_NAME_STATE + TEXT_TYPE + COMMA_SEP +
                    DbContract.DbArticle.COLUMN_NAME_TIMESTAMP + TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DbContract.DbArticle.TABLE_NAME;


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}


