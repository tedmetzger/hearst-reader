package com.tedmetzger.hearstreader.app.adapter;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tedmetzger.hearstreader.app.R;
import com.tedmetzger.hearstreader.app.core.Article;
import com.tedmetzger.hearstreader.app.core.ArticleState;
import com.tedmetzger.hearstreader.app.core.TouchDelegate;
import com.tedmetzger.hearstreader.app.manager.ArticleImageManager;
import com.tedmetzger.hearstreader.app.network.DataApi;
import com.tedmetzger.hearstreader.app.task.DownloadImageTask;
import com.tedmetzger.hearstreader.app.utils.ViewUtil;
import com.tedmetzger.hearstreader.app.x.SwipeDismissListViewTouchListener;

import java.util.List;


/**
 * Created by reneravenel on 8/29/14.
 */
public class ArticleListAdapter extends ArrayAdapter<Article> implements TouchDelegate {

    private static String TAG = ArticleListAdapter.class.getSimpleName();
    private ArticleImageManager articleImageManager = ArticleImageManager.INSTANCE;

    private final Context context;
    private final LayoutInflater mInflater;

    public ArticleListAdapter(Context context) {
        super(context, 0);
        this.context = context;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final Article article = getItem(position);

        ArticleViewHolder articleViewHolder;
        if (convertView == null || convertView.getTag() == null) {
            convertView = mInflater.inflate(R.layout.article_item_row, null);

            articleViewHolder = createViewHolder(convertView);

            convertView.setTag(articleViewHolder);
        }
        else {
            articleViewHolder = (ArticleViewHolder) convertView.getTag();
        }

        articleViewHolder.title.setTypeface(articleViewHolder.title.getTypeface(), Typeface.BOLD);
        articleViewHolder.title.setTextColor(getContext().getResources().getColor(R.color.default_dark_text));

        if (article.getState().contains(ArticleState.read)) {
           applyReadStyle(articleViewHolder.title);
        }

        String since =  String.valueOf(article.getHoursSincePub() / 1) + "h";
        articleViewHolder.title.setText(article.getTitle());
        articleViewHolder.ago.setText(since);
        articleViewHolder.score.setText(String.valueOf(article.getCommentScore()));

        String imageUrl = article.getImageUrl();
        articleViewHolder.image.setVisibility(View.INVISIBLE);

        new DownloadImageTask(articleViewHolder.image, articleImageManager).execute(imageUrl);

        String commentCount = "";
        int commentCounts = article.getCommentCount();
        if(commentCounts > 0){
            commentCount = String.valueOf(article.getCommentCount() );
        }

        articleViewHolder.commentsCount.setText(commentCount);

        if (0 < article.getSaveTimestamp()) {
            articleViewHolder.saveBtn.setImageResource(R.drawable.icon_saved);
        }
        else {
            articleViewHolder.saveBtn.setImageResource(R.drawable.icon_save_light);
        }

        return convertView;
    }

    class ArticleViewHolder {
        TextView title;
        TextView ago;
        TextView score;
        TextView commentsCount;
        LinearLayout scoreWrap;
        LinearLayout commentsWrap;
        ImageView commentsBtn;
        ImageView image;
        ImageView saveBtn;
        ImageView shareBtn;
        LinearLayout contentContainer;
    }


    private ArticleViewHolder createViewHolder(View parentView) {
        final ArticleViewHolder articleViewHolder = new ArticleViewHolder();
        articleViewHolder.title = (TextView) parentView.findViewById(R.id.title);
        articleViewHolder.commentsCount = (TextView) parentView.findViewById(R.id.commentsCount);
        articleViewHolder.score = (TextView) parentView.findViewById(R.id.score);
        articleViewHolder.commentsWrap = (LinearLayout)parentView.findViewById(R.id.commentsWrap);
        articleViewHolder.scoreWrap = (LinearLayout)parentView.findViewById(R.id.scoreWrap);
        articleViewHolder.image = (ImageView) parentView.findViewById(R.id.image);
        articleViewHolder.ago = (TextView) parentView.findViewById(R.id.ago);
        articleViewHolder.saveBtn = (ImageView) parentView.findViewById(R.id.btn_save);
        articleViewHolder.shareBtn = (ImageView) parentView.findViewById(R.id.btn_share);
        articleViewHolder.contentContainer = (LinearLayout) parentView.findViewById(R.id.article_content_container);
        articleViewHolder.commentsBtn = (ImageView) parentView.findViewById(R.id.btn_comments);
        return  articleViewHolder;
    }

    private void applyReadStyle(TextView textView) {
        textView.setTextColor(getContext().getResources().getColor(R.color.dk_gray));
        textView.setTypeface(textView.getTypeface(), Typeface.NORMAL);
    }

    //-- Interface Methods --//

    public void onTouch(ViewGroup vg, MotionEvent motionEvent, int position) {
        float meRawX = motionEvent.getRawX();
        float meRawY = motionEvent.getRawY();

        Article article = (Article) getItem(position);

        List<View> childViews = SwipeDismissListViewTouchListener.getAllChildred(vg);
        for (View v : childViews) {
            int[] viewCoords = new int[2];
            v.getLocationOnScreen(viewCoords);

            Rect rect = new Rect(0, 0, v.getWidth(), v.getHeight());
            rect.offset(viewCoords[0], viewCoords[1]);

            if (!rect.contains((int) meRawX, (int) meRawY)) {
                continue;
            }

            boolean handled = false;
            switch(v.getId()) {
                case R.id.image:
                    DataApi.markAsRead(article);
                    ViewUtil.getWebViewArticle(this, false, article, context);
                    handled = true;
                    break;
                case R.id.title:
                    DataApi.markAsRead(article);
                    ViewUtil.getWebViewArticle(this, false, article, context);
                    handled = true;
                    break;
                case R.id.commentsWrap:
                    DataApi.markAsRead(article);
                    ViewUtil.getWebViewArticle(this, true, article, context);
                    handled = true;
                    break;
                case R.id.btn_save:
                    if (0 < article.getSaveTimestamp()) {
                        ((ImageView) v).setImageResource(R.drawable.icon_save_light);
                        DataApi.unSaveArticle(article);
                    }
                    else {
                        ((ImageView) v).setImageResource(R.drawable.icon_saved);
                        DataApi.saveArticle(article);
                    }
                    handled = true;
                    break;
                case R.id.btn_share:
                    ViewUtil.startShareDialog((Activity) context, "");
                    handled = true;
                    break;
                default:
            }

            if (handled) {
                break;
            }
        }
    }

}
