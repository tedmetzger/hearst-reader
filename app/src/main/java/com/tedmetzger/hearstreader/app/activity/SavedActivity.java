package com.tedmetzger.hearstreader.app.activity;

import android.app.ActionBar;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.tedmetzger.hearstreader.app.R;
import com.tedmetzger.hearstreader.app.adapter.SavedArticleAdapter;
import com.tedmetzger.hearstreader.app.core.Article;
import com.tedmetzger.hearstreader.app.network.DataApi;
import com.tedmetzger.hearstreader.app.utils.ViewUtil;
import com.tedmetzger.hearstreader.app.x.SwipeDismissListViewTouchListener;

/**
 * Created by reneravenel on 9/2/14.
 */
public class SavedActivity extends ListActivity {

    private SavedArticleAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionbar = getActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        ViewUtil.initAppFonts(this);
        setContentView(R.layout.activity_saved);

        listAdapter = new SavedArticleAdapter(this);
        setListAdapter(listAdapter);

        setListeners();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (listAdapter == null) return;

        listAdapter.clear();
        listAdapter.addAll(DataApi.getSavedArticles(0, 0));
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setListeners() {
        ListView listView = getListView();
        SwipeDismissListViewTouchListener swipeListener = new SwipeDismissListViewTouchListener(listView,  new SwipeDismissListViewTouchListener.DismissCallbacks() {
            @Override
            public boolean canDismiss(int position) {
                return true;
            }

            @Override
            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions) {
                    Article article = listAdapter.getItem(position);
                    DataApi.unSaveArticle(article);
                    listAdapter.remove(article);
                }
                listAdapter.notifyDataSetChanged();
            }
            @Override
            public void onTouch(ViewGroup vg, MotionEvent motionEvent,int position) {
                listAdapter.onTouch(vg, motionEvent, position);
            }

        });

        listView.setOnTouchListener(swipeListener);
        listView.setOnScrollListener(swipeListener.makeScrollListener());
    }

}
