package com.tedmetzger.hearstreader.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.tedmetzger.hearstreader.app.application.AppConstants;

/**
 * Created by reneravenel on 8/31/14.
 */
public class SharedPrefsUtility {

    public static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
        //return context.getSharedPreferences(AppConstants.PREFS_APP_NAME, Context.MODE_PRIVATE);
    }

    public static void setPrefsAlarm(Context context) {
        SharedPreferences.Editor prefEditor = getPreferences(context).edit();
        prefEditor.putBoolean(AppConstants.PREFS_HAS_SET_ALARM, true);
        prefEditor.commit();
    }

    public static boolean getPrefsAlarm(Context context) {
        SharedPreferences prefs = getPreferences(context);
        return prefs.getBoolean(AppConstants.PREFS_HAS_SET_ALARM, false);
    }
}
