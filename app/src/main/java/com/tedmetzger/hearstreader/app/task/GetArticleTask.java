package com.tedmetzger.hearstreader.app.task;

/**
 * Created by jamesmetzger on 8/12/14.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.tedmetzger.hearstreader.app.core.Article;
import com.tedmetzger.hearstreader.app.core.ArticleCache;
import com.tedmetzger.hearstreader.app.core.Channel;
import com.tedmetzger.hearstreader.app.core.ViafouraJsonArticle;
import com.tedmetzger.hearstreader.app.manager.ArticleJsonManager;
import com.tedmetzger.hearstreader.app.manager.FeedManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetArticleTask extends AsyncTask<Void, Void, Article> {

    private static String TAG = GetArticleTask.class.getSimpleName();

    private static final String URL_SFGATE = "www.sfgate.com";
    private static final String PROTOCOL_HTTP = "http://";

    ArticleJsonManager articleJsonManager = ArticleJsonManager.INSTANCE;

    Context ctx;
    String url;
    Channel channel;

    String path;

    public GetArticleTask(Context ctx, String url, Channel channel){
        this.ctx = ctx;
        this.url = url;
        this.channel = channel;

        this.path = url.replace(PROTOCOL_HTTP + URL_SFGATE, "");
    }

    @Override
    protected Article doInBackground(Void... voids) {
        Article result = null;
        try {
            String articleJson = getArticleJson();
            result = parse(articleJson);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getArticleJson() throws IOException {
        InputStream in = null;
        String jsonUrl = "https://api.viafoura.com/v2/?json={%22site%22:%22" + URL_SFGATE + "%22,%22requests%22:{%221%22:{%22title%22:%22Foo%22,%22path%22:%22" + path  + "%22,%22verb%22:%22post%22,%22route%22:%22/pages%22}}}";
        String articleJson = articleJsonManager.getArticleJsonForUrl(jsonUrl) ;

        if(articleJson == null) {
            try {
                //TODO: unhardcode this
                URL url = new URL(jsonUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                in = conn.getInputStream();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                for (int count; (count = in.read(buffer)) != -1; ) {
                    out.write(buffer, 0, count);
                }
                byte[] response = out.toByteArray();
                articleJson = new String(response, "UTF-8");
                articleJsonManager.setArticleJsonUrl(jsonUrl,articleJson);
            } finally {
                if (in != null) {
                    in.close();

                }
            }
        }
        return articleJson;
    }

    private Article parse(String articleJson) throws IOException {
        //this is kind of retarded - Java ain't down w/ numbers for variable names: "1"
        String modArticleJson = articleJson.replace("{\"responses\":{\"1\":{","{\"responses\":{\"first\":{");

        ViafouraJsonArticle viafouraArticle = new Gson().fromJson(modArticleJson, ViafouraJsonArticle.class);
        Article article = new Article(this.url, viafouraArticle, this.channel);

        if (null == article.getTitle() || "".equals(article.getTitle())) {
            article = null;
        }

        return article;
    }

    @Override
    protected void onPostExecute(final Article article) {
        if (article != null) {
            ArticleCache.INSTANCE.addArticle(article);
        }
        FeedManager.INSTANCE.decrementArticleTaskCount();
    }
}


