package com.tedmetzger.hearstreader.app.manager;

import android.graphics.Bitmap;

import java.util.HashMap;

/**
 * Created by jamesmetzger on 8/14/14.
 */
public class ArticleJsonManager {

        private HashMap<String,String> urlsToArticleJson = new HashMap<String, String>();

        public static ArticleJsonManager INSTANCE = new ArticleJsonManager();

        private ArticleJsonManager() {}

        public String getArticleJsonForUrl(String url) {
            return urlsToArticleJson.get(url);
        }

        public void setArticleJsonUrl(String url, String json) {
             urlsToArticleJson.put(url,json);
        }
    }

