package com.tedmetzger.hearstreader.app.manager;

import com.tedmetzger.hearstreader.app.application.HearstReaderApplication;
import com.tedmetzger.hearstreader.app.core.Channel;
import com.tedmetzger.hearstreader.app.task.GetRssFeedTask;

/**
 * Created by reneravenel on 9/2/14.
 */
public class FeedManager {

    public static FeedManager INSTANCE = new FeedManager();

    private FeedRefreshListener listener = null;
    private int articleTaskCount = 0;
    private boolean refreshInProgress = false;

    private FeedManager() {}

    public void refreshFeed() {
        refreshInProgress = true;
        for (Channel channel : Channel.values()) {
            new GetRssFeedTask(HearstReaderApplication.getContext(), channel).execute();
        }
    }

    public void incrementArticleTaskCount() {
        articleTaskCount++;
    }

    public void decrementArticleTaskCount() {
        if (0 < articleTaskCount) {
            articleTaskCount--;
        }

        if (refreshInProgress && 1 >= articleTaskCount) {
            refreshInProgress = false;

            if (null != listener) {
                listener.onFeedRefresh();
            }
        }
    }

    public void setRefreshListener(FeedRefreshListener listener) {
        this.listener = listener;
    }

    public interface FeedRefreshListener {

        void onFeedRefresh();

    }
}
