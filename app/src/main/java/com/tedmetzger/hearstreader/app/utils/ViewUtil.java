package com.tedmetzger.hearstreader.app.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.ArrayAdapter;

import com.tedmetzger.hearstreader.app.R;
import com.tedmetzger.hearstreader.app.adapter.ArticleListAdapter;
import com.tedmetzger.hearstreader.app.application.AppConstants;
import com.tedmetzger.hearstreader.app.core.Article;

/**
 * Created by ameliacatalano on 8/28/14.
 */
public class ViewUtil {

    public static void initAppFonts(Context context) {
        FontsOverride.setDefaultFont(context, "MONOSPACE", "Roboto-Light.ttf");
        FontsOverride.setDefaultFont(context, "DEFAULT", "Roboto-Light.ttf");
        FontsOverride.setDefaultFont(context, "SANS_SERIF", "Roboto-Light.ttf");
    }

    public static void getWebViewArticle(ArrayAdapter adapter,
                                         boolean comment, Article article, Context context){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(article.getUrl() + (comment ? "#comments": "" )));
        context.startActivity(browserIntent);
        adapter.notifyDataSetChanged();
    }


    public static void startShareDialog(Activity activity, String itemUrl) {
        Context context = activity;
        String shareUrl = itemUrl.replace(" ", "-");
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
        activity.startActivityForResult(Intent.createChooser(shareIntent, context.getString(R.string.share_dialog_title)),
               AppConstants.INTENT_REQUEST_SHARE_FEED_ITEM);
    }
}
