package com.tedmetzger.hearstreader.app.network;

import android.content.SharedPreferences;
import android.os.SystemClock;

import com.tedmetzger.hearstreader.app.application.HearstReaderApplication;
import com.tedmetzger.hearstreader.app.core.Article;
import com.tedmetzger.hearstreader.app.core.ArticleCache;
import com.tedmetzger.hearstreader.app.core.ArticleState;
import com.tedmetzger.hearstreader.app.core.Channel;
import com.tedmetzger.hearstreader.app.utils.SharedPrefsUtility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by reneravenel on 8/31/14.
 */
public class DataApi {

    public static List<Article> getFeedByVelocity(int startingIndex, int pageSize) {
        List<Article> result = getCleanFeed();
        Collections.sort(result);

        return result;
    }

    public static List<Article> getFeedByRecency(int startingIndex, int pageSize) {
        List<Article> result = getCleanFeed();

        Collections.sort(result, new Comparator<Article>() {
            @Override
            public int compare(Article a, Article b) {
                return new Integer(b.getTimestamp()).compareTo(a.getTimestamp());
            }
        });

        return result;
    }

    public static List<Article> getFeedByComments(int startingIndex, int pageSize) {
        List<Article> result = getCleanFeed();

        Collections.sort(result, new Comparator<Article>() {
            @Override
            public int compare(Article a, Article b) {
                return new Integer(b.getCommentCount()).compareTo(a.getCommentCount());
            }
        });

        return result;
    }

    public static List<Article> getSavedArticles(int startingIndex, int pageSize) {
        List<Article> result = getCleanFeed();

        for (Iterator<Article> it = result.iterator(); it.hasNext();) {
            Article article = it.next();
            if (0 == article.getSaveTimestamp()) {
                it.remove();
            }
        }

        Collections.sort(result, new Comparator<Article>() {
            @Override
            public int compare(Article a, Article b) {
                return new Long(b.getSaveTimestamp()).compareTo(a.getSaveTimestamp());
            }
        });

        return result;
    }

    public static void saveArticle(Article article) {
        article.setSaveTimestamp(System.currentTimeMillis());
        ArticleCache.INSTANCE.updateArticleState(article);
    }

    public static void unSaveArticle(Article article) {
        article.setSaveTimestamp(0L);
        ArticleCache.INSTANCE.updateArticleState(article);
    }

    public static void markAsRead(Article article) {
        article.getState().add(ArticleState.read);
        ArticleCache.INSTANCE.updateArticleState(article);
    }

    public static void dismissFromFeed(Article article) {
        article.getState().add(ArticleState.dismissed);
        ArticleCache.INSTANCE.updateArticleState(article);
    }

    @Deprecated
    public static void scrubChannel(Channel channel) {
        ArticleCache.INSTANCE.scrubChannel(channel);
    }

    private static List<Article> getCleanFeed() {
        List<Article> result = new ArrayList<Article>(ArticleCache.INSTANCE.getArticles());

        Set<Channel> removeChannels = new HashSet<Channel>();
        SharedPreferences preferences = SharedPrefsUtility.getPreferences(HearstReaderApplication.getContext());
        for (Channel channel : Channel.values()) {
            if (!preferences.getBoolean(channel.getName(), true)) {
                removeChannels.add(channel);
            }
        }

        for (Iterator<Article> it = result.iterator(); it.hasNext();) {
            Article article = it.next();

            if (removeDismissed(article)) {
                it.remove();
                continue;
            }

            if (removeChannels(article, removeChannels)) {
                it.remove();
            }
        }

        return result;
    }

    private static boolean removeDismissed(Article article) {
        return article.getState().contains(ArticleState.dismissed);
    }

    private static boolean removeChannels(Article article, Set<Channel> channels) {
        return channels.contains(article.getChannel());
    }

}
