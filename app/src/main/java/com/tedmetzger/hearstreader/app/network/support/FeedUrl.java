package com.tedmetzger.hearstreader.app.network.support;

public class FeedUrl {

    public static String URL_SFGATE_RSS_TOP_NEWS = "http://www.sfgate.com/rss/feed/Top-News-Stories-595.php";
    public static String URL_SFGATE_RSS_ENTERTAINMENT = "http://www.sfgate.com/rss/feed/Entertainment-530.php";
    public static String URL_SFGATE_RSS_POLITICS = "http://www.sfgate.com/rss/feed/Politics-RSS-Feed-436.php";
    public static String URL_SFGATE_RSS_BAY_NEWS = "http://www.sfgate.com/bayarea/feed/Bay-Area-News-429.php";
    public static String URL_SFGATE_RSS_SPORTS = "http://www.sfgate.com/rss/feed/Top-Sports-Stories-RSS-Feed-487.php";
}
