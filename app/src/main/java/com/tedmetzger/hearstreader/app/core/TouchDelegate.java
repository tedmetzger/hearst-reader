package com.tedmetzger.hearstreader.app.core;

import android.view.MotionEvent;
import android.view.ViewGroup;

/**
 * Created by reneravenel on 9/2/14.
 */
public interface TouchDelegate {

    void onTouch(ViewGroup vg, MotionEvent motionEvent, int position);

}
