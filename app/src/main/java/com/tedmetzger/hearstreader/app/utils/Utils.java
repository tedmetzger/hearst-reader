package com.tedmetzger.hearstreader.app.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.tedmetzger.hearstreader.app.application.HearstReaderApplication;
import com.tedmetzger.hearstreader.app.network.DataService;

/**
 * Created by reneravenel on 8/31/14.
 */
public class Utils {

    public static void startDataService() {
        Intent intent = new Intent(HearstReaderApplication.getInstance(), DataService.class);
        HearstReaderApplication.getContext().startService(intent);
    }

    public static void setAlarmService() {
        boolean alarmSet = SharedPrefsUtility.getPrefsAlarm(HearstReaderApplication.getContext());

        if (!alarmSet) {
            Intent intent = new Intent(HearstReaderApplication.getInstance(), DataService.class);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(HearstReaderApplication.getContext(), 0, intent, 0);

            AlarmManager alarmManager = (AlarmManager) HearstReaderApplication.getContext().getSystemService(Context.ALARM_SERVICE);
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                    AlarmManager.INTERVAL_HOUR,
                    AlarmManager.INTERVAL_HOUR,
                    alarmIntent);

            SharedPrefsUtility.setPrefsAlarm(HearstReaderApplication.getContext());
        }
    }
}
