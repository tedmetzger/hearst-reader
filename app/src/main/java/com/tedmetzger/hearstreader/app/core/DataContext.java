package com.tedmetzger.hearstreader.app.core;

import android.util.DisplayMetrics;

/**
 * Created by reneravenel on 8/29/14.
 */
public class DataContext {

    private static DisplayMetrics metrics = null;
    private static int textWidth = 0;

    public static DisplayMetrics getMetrics() {
        return metrics;
    }

    public static void setMetrics(DisplayMetrics metrics) {
        DataContext.metrics = metrics;
    }

    public static int getTextWidth() {
        return textWidth;
    }

    public static void setTextWidth(int textWidth) {
        DataContext.textWidth = textWidth;
    }
}
