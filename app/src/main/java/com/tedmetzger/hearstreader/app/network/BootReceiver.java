package com.tedmetzger.hearstreader.app.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tedmetzger.hearstreader.app.utils.Utils;

/**
 * Created by reneravenel on 8/31/14.
 */
public class BootReceiver extends BroadcastReceiver {

    private static final String TAG = BootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "BootReceiver Starting");

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Utils.startDataService();
            Utils.setAlarmService();
        }

        Log.i(TAG, "BootReceiver Started");
    }

}
