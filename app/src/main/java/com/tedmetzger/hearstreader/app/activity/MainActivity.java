package com.tedmetzger.hearstreader.app.activity;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.tedmetzger.hearstreader.app.R;
import com.tedmetzger.hearstreader.app.adapter.ArticleListAdapter;
import com.tedmetzger.hearstreader.app.core.Article;
import com.tedmetzger.hearstreader.app.core.FeedSort;
import com.tedmetzger.hearstreader.app.manager.FeedManager;
import com.tedmetzger.hearstreader.app.network.DataApi;
import com.tedmetzger.hearstreader.app.utils.ViewUtil;
import com.tedmetzger.hearstreader.app.x.SwipeDismissListViewTouchListener;


public class MainActivity extends ListActivity implements FeedManager.FeedRefreshListener {

    private static String TAG = MainActivity.class.getSimpleName();

    private ArticleListAdapter articleListAdapter;
    private FeedSort sort = FeedSort.velocity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtil.initAppFonts(this);
        setContentView(R.layout.activity_main);

        articleListAdapter = new ArticleListAdapter(this);
        setListAdapter(articleListAdapter);
        setListeners();

        setupActionBar();
    }

    private void setupActionBar() {
        View mCustomView = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
        ActionBar actionbar = getActionBar();
        actionbar.setCustomView(mCustomView);
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setDisplayShowHomeEnabled(true);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setDisplayUseLogoEnabled(false);
        setTitle("");

        setupSortSpinner();
    }

    private void setupSortSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.sorting_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.sorting_options_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setDropDownVerticalOffset(10);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = (TextView) parent.getSelectedView();
                tv.setText("");

                switch (position) {
                    case 0:
                    default:
                        sort = FeedSort.velocity;
                        break;
                    case 1:
                        sort = FeedSort.latest;
                        break;
                    case 2:
                        sort = FeedSort.comment;
                        break;
                }

                refreshFeed();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                ((TextView) parent.getSelectedView()).setText("");


            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshFeed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (item.getItemId() == R.id.news_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        else if (item.getItemId() == R.id.saved_articles) {
            Intent intent = new Intent(this, SavedActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setListeners() {
        FeedManager.INSTANCE.setRefreshListener(this);

        final ListView listView = getListView();
        SwipeDismissListViewTouchListener swipeListener = new SwipeDismissListViewTouchListener(listView,  new SwipeDismissListViewTouchListener.DismissCallbacks() {
            @Override
            public boolean canDismiss(int position) {
                return true;
            }

            @Override
            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions) {
                    Article article = articleListAdapter.getItem(position);
                    DataApi.dismissFromFeed(article);
                    articleListAdapter.remove(article);
                }
                articleListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onTouch(ViewGroup vg, MotionEvent motionEvent, int position) {
                articleListAdapter.onTouch(vg, motionEvent, position);
            }

        });

        listView.setOnTouchListener(swipeListener);
        listView.setOnScrollListener(swipeListener.makeScrollListener());

    }

    private void refreshFeed() {
        if (articleListAdapter == null) return;
        articleListAdapter.clear();
        articleListAdapter.notifyDataSetChanged();

        switch(sort) {
            case velocity:
                articleListAdapter.addAll(DataApi.getFeedByVelocity(0, 0));
                break;
            case latest:
                articleListAdapter.addAll(DataApi.getFeedByRecency(0, 0));
                break;
            case comment:
                articleListAdapter.addAll(DataApi.getFeedByComments(0, 0));
        }


        articleListAdapter.notifyDataSetChanged();
        //getListView().smoothScrollToPosition(0);
        getListView().setSelection(0);
    }

    //-- Interface Methods --//

    public void onFeedRefresh() {
        refreshFeed();
    }

}