package com.tedmetzger.hearstreader.app.db;

import android.provider.BaseColumns;

/**
 * Created by reneravenel on 8/28/14.
 */
public final class DbContract {

    private DbContract() {}

    public static abstract class DbArticle implements BaseColumns {

        public static final String TABLE_NAME = "article";

        public static final String COLUMN_NAME_FEED_ID = "feedId";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_COMMENT_COUNT = "commentCount";
        public static final String COLUMN_NAME_URL = "url";
        public static final String COLUMN_NAME_IMAGE_URL = "imageUrl";
        public static final String COLUMN_NAME_IMAGE_FILE = "imageFile";
        public static final String COLUMN_NAME_ARTICLE_FILE = "articleFile";
        public static final String COLUMN_NAME_STATE = "state";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";

    }
}
