package com.tedmetzger.hearstreader.app.application;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.tedmetzger.hearstreader.app.network.DataService;
import com.tedmetzger.hearstreader.app.utils.Utils;

/**
 * Created by reneravenel on 8/29/14.
 */
public class HearstReaderApplication extends Application {

    private static HearstReaderApplication INSTANCE = null;
    private static Context CONTEXT;

    public HearstReaderApplication() {
        super();
        INSTANCE = this;
    }

    public static HearstReaderApplication getInstance() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CONTEXT = getApplicationContext();

        Utils.startDataService();
        Utils.setAlarmService();
    }

    public static Context getContext() {
        return CONTEXT;
    }
}