package com.tedmetzger.hearstreader.app.task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.tedmetzger.hearstreader.app.manager.ArticleImageManager;
import com.tedmetzger.hearstreader.app.core.UrlAndBitmap;

import java.io.InputStream;


/**
 * Created by jamesmetzger on 8/13/14.
 */
public class DownloadImageTask extends AsyncTask<String, Void, UrlAndBitmap> {
    ImageView bmImage;
    ArticleImageManager articleImageManager;

    public DownloadImageTask(ImageView bmImage, ArticleImageManager articleImageManager) {
        this.bmImage = bmImage;
        this.articleImageManager = articleImageManager;
    }

    protected UrlAndBitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        UrlAndBitmap urlAndBitmap = null;
        Bitmap cachedImg = articleImageManager.getBitmapForUrl(urldisplay);
        if(cachedImg == null) {

            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                Bitmap img = BitmapFactory.decodeStream(in);
                urlAndBitmap = new UrlAndBitmap(urldisplay, img);
                articleImageManager.setBitmapForUrl(urldisplay,img);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        } else{

            urlAndBitmap = new UrlAndBitmap(urldisplay, cachedImg);

        }
        return urlAndBitmap;
    }

    protected void onPostExecute(UrlAndBitmap result) {

        if (result == null) return;
        bmImage.setImageBitmap(result.getBitmap());
        bmImage.setVisibility(View.VISIBLE);
    }
}