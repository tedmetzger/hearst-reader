package com.tedmetzger.hearstreader.app.core;

import android.content.Context;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by reneravenel on 8/31/14.
 */
public class ArticleCache {

    private static Set<Article> CACHE = new HashSet<Article>();

    public static ArticleCache INSTANCE = new ArticleCache();

    private ArticleCache() {}

    public Set<Article> getArticles() {
        synchronized (CACHE) {
            return new HashSet<Article>(CACHE);
        }
    }

    public void addArticle(Article article) {
        if (null == article) {
            return;
        }

        synchronized (CACHE) {
            if (!CACHE.contains(article)) {
                CACHE.add(article);
            }
        }
    }

    public void updateArticleState(Article article) {
        if (null != article) {
            synchronized (CACHE) {
                if (CACHE.contains(article)) {
                    for (Article a : CACHE) {
                        if (a.equals(article)) {
                            a.setState(article.getState());
                            return;
                        }
                    }
                }
                CACHE.add(article);
            }
        }
    }

    @Deprecated
    public void scrubChannel(Channel channel) {
        synchronized (CACHE) {
            for (Iterator<Article> it = CACHE.iterator(); it.hasNext();) {
                Article article = it.next();
                if (article.getChannel() == channel) {
                    it.remove();
                }
            }
        }
    }
}
