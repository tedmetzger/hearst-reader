package com.tedmetzger.hearstreader.app.core;

import android.graphics.Bitmap;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by jamesmetzger on 8/12/14.
 */
public class Article implements Comparable<Article>{

    long dbId;
    String feedId;
    String title;
    int commentCount;
    String url;
    String imageUrl;
    String imageFile;
    Bitmap image;
    String articleFile;
    Set<ArticleState> state = new HashSet<ArticleState>();
    int timestamp;
    Channel channel;
    long saveTimestamp = 0L;

    public Article(String url, ViafouraJsonArticle viafouraArticle, Channel channel) {
        this.url = url;
        this.title = viafouraArticle.getTitle();
        this.commentCount = viafouraArticle.getCommentCount();
        this.feedId = viafouraArticle.getId();
        this.imageUrl = viafouraArticle.getImageUrl();
        this.timestamp = viafouraArticle.getTimestamp();
        this.channel = channel;
    }

    public Article(String url, int commentCount){
        this.commentCount = commentCount;
        this.url = url;
    }

    private float getCommentsPerHour(){

        float hours = getHoursSincePub();
        if(hours < 0.5f){
            hours = 0.5f;
        }
        float ratio = getCommentCount()/hours;

        return ratio;
    }

    public int getCommentScore(){
        int score = Math.round(getCommentsPerHour() * 100);
        return score;
    }

    public int getHoursSincePub(){

        int now = (int) (System.currentTimeMillis() / 1000L);
        int secondsSincePub = now - timestamp;

        return secondsSincePub/3600;
    }

    public int getStateBitMask() {
        int result = 0;
        for (ArticleState s : state) {
            result = result | s.getBitMask();
        }

        return result;
    }

    public void setHidden(boolean hidden) {
        if (hidden) {
            state.add(ArticleState.dismissed);
        }
        else {
            state.remove(ArticleState.dismissed);
        }
    }

    public boolean isHidden() {
        return state.contains(ArticleState.dismissed);
    }

    //-- Getters, Setters --//


    public long getDbId() {
        return dbId;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getArticleFile() {
        return articleFile;
    }

    public void setArticleFile(String articleFile) {
        this.articleFile = articleFile;
    }

    public void setState(Collection<ArticleState> state) {
        if (null != state) {
            this.state = new HashSet<ArticleState>(state);
        }
        else {
            this.state.clear();
        }
    }

    public Set<ArticleState> getState() {
        return this.state;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public long getSaveTimestamp() {
        return saveTimestamp;
    }

    public void setSaveTimestamp(long saveTimestamp) {
        this.saveTimestamp = saveTimestamp;
    }

    //-- Interface Methods --//

    @Override
    public int compareTo(Article a) {
        //highest score first
        return a.getCommentScore() - this.getCommentScore();
    }
}

