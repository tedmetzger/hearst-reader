package com.tedmetzger.hearstreader.app.core;

import android.graphics.Bitmap;

/**
 * Created by jamesmetzger on 8/13/14.
 */
public class UrlAndBitmap {
    String url;
    Bitmap bitmap;

    public UrlAndBitmap(String url, Bitmap bitmap){
        this.url = url;
        this.bitmap = bitmap;
    }

    public String getUrl() {
        return url;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
