package com.tedmetzger.hearstreader.app.application;

public class AppConstants {

    /* ====== SHARED PREFERENCES ====== */
    public static final String PREFS_APP_NAME = "com.tedmetzger.hearstreader";
    public static final String PREFS_HAS_SET_ALARM = "hasSetAlarm";

    /*== FEED SETTINGS ==*/
    public static String SETTINGS_TOP_NEWS = "top_news_checkbox";
    public static String SETTINGS_ENTERTAINMENT = "entertainment_checkbox";
    public static String SETTINGS_POLITICS = "politics_checkbox";
    public static String SETTINGS_BAY_NEWS = "bay_area_news_checkbox";
    public static String SETTINGS_SPORTS = "sports_checkbox";


    /*== ==*/
    public static final int INTENT_REQUEST_SHARE_FEED_ITEM = 7001;

}
