package com.tedmetzger.hearstreader.app.core;

/**
 * Created by reneravenel on 8/28/14.
 */
public enum ArticleState {

    //saved              (0x1), //implemented via timestamp
    read              (0x10),
    dismissed        (0x100);

    private final int bitMask;

    private ArticleState(int bitMask) {
        this.bitMask = bitMask;
    }

    public int getBitMask() {
        return this.bitMask;
    }
}
