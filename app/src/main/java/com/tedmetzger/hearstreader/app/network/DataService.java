package com.tedmetzger.hearstreader.app.network;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.tedmetzger.hearstreader.app.application.AppConstants;
import com.tedmetzger.hearstreader.app.core.Channel;
import com.tedmetzger.hearstreader.app.manager.FeedManager;
import com.tedmetzger.hearstreader.app.network.support.FeedUrl;
import com.tedmetzger.hearstreader.app.task.GetRssFeedTask;

/**
 * Created by reneravenel on 8/31/14.
 */
public class DataService extends Service {

    private static final String TAG = DataService.class.getSimpleName();

    private static DataService INSTANCE = null;

    public DataService() {
        INSTANCE = this;
    }

    public static DataService getInstance() {
        return INSTANCE;
    }

    // -- Service methods -------------------------------------------//

    @Override
    public void onCreate() {
        Log.i(TAG, "Starting DataService.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received start id " + startId + ": " + intent);

        FeedManager.INSTANCE.refreshFeed();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Stopping DataService.");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
