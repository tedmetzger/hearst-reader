package com.tedmetzger.hearstreader.app.core;

import com.tedmetzger.hearstreader.app.application.AppConstants;
import com.tedmetzger.hearstreader.app.network.support.FeedUrl;

/**
 * Created by reneravenel on 9/2/14.
 */
public enum Channel {

    topNews(FeedUrl.URL_SFGATE_RSS_TOP_NEWS, AppConstants.SETTINGS_TOP_NEWS),
    entertainment(FeedUrl.URL_SFGATE_RSS_ENTERTAINMENT, AppConstants.SETTINGS_ENTERTAINMENT),
    politics(FeedUrl.URL_SFGATE_RSS_POLITICS, AppConstants.SETTINGS_POLITICS),
    bayNews(FeedUrl.URL_SFGATE_RSS_BAY_NEWS, AppConstants.SETTINGS_BAY_NEWS),
    sports(FeedUrl.URL_SFGATE_RSS_SPORTS, AppConstants.SETTINGS_SPORTS);

    private String name;
    private String url;

    private Channel(String url, String name) {
        this.url = url;
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
