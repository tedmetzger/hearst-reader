package com.tedmetzger.hearstreader.app.adapter;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tedmetzger.hearstreader.app.R;
import com.tedmetzger.hearstreader.app.core.Article;
import com.tedmetzger.hearstreader.app.core.ArticleState;
import com.tedmetzger.hearstreader.app.core.TouchDelegate;
import com.tedmetzger.hearstreader.app.manager.ArticleImageManager;
import com.tedmetzger.hearstreader.app.network.DataApi;
import com.tedmetzger.hearstreader.app.task.DownloadImageTask;
import com.tedmetzger.hearstreader.app.utils.ViewUtil;
import com.tedmetzger.hearstreader.app.x.SwipeDismissListViewTouchListener;

import java.util.List;

/**
 * Created by reneravenel on 9/2/14.
 */
public class SavedArticleAdapter extends ArrayAdapter<Article> implements TouchDelegate {

    private static String TAG = SavedArticleAdapter.class.getSimpleName();

    private ArticleImageManager articleImageManager = ArticleImageManager.INSTANCE;

    private final Context context;
    private final LayoutInflater mInflater;


    public SavedArticleAdapter(Context context) {
        super(context, 0);
        this.context = context;

        mInflater = LayoutInflater.from(context);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final SavedArticleAdapter adapter = (SavedArticleAdapter) ((ListActivity) context).getListAdapter();

        ArticleViewHolder articleViewHolder;

        if (convertView == null || convertView.getTag() == null) {
            articleViewHolder = new ArticleViewHolder();
            convertView = mInflater.inflate(R.layout.saved_article_row, null);
            articleViewHolder.title = (TextView) convertView.findViewById(R.id.title);
            articleViewHolder.commentsCount = (TextView) convertView.findViewById(R.id.commentsCount);
            articleViewHolder.score = (TextView) convertView.findViewById(R.id.score);
            articleViewHolder.commentsWrap = (LinearLayout)convertView.findViewById(R.id.commentsWrap);
            articleViewHolder.scoreWrap = (LinearLayout)convertView.findViewById(R.id.scoreWrap);
            articleViewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            articleViewHolder.ago = (TextView) convertView.findViewById(R.id.ago);
            articleViewHolder.contentContainer = (LinearLayout) convertView.findViewById(R.id.article_content_container);
            articleViewHolder.shareBtn = (ImageView) convertView.findViewById(R.id.btn_share);
            articleViewHolder.commentsBtn = (ImageView) convertView.findViewById(R.id.btn_comments);
            convertView.setTag(articleViewHolder);
        }

        articleViewHolder = (ArticleViewHolder) convertView.getTag();

        final Article article = getItem(position);

        articleViewHolder.title.setTypeface(articleViewHolder.title.getTypeface(), Typeface.BOLD);
        articleViewHolder.title.setTextColor(getContext().getResources().getColor(R.color.default_dark_text));

        if (article.getState().contains(ArticleState.read)) {
            applyReadStyle(articleViewHolder.title);
        }

        String since =  String.valueOf(article.getHoursSincePub() / 1) + "h";
        articleViewHolder.title.setText(article.getTitle());
        articleViewHolder.ago.setText(since);
        articleViewHolder.score.setText(String.valueOf(article.getCommentScore()));

        String imageUrl = article.getImageUrl();
        articleViewHolder.image.setVisibility(View.INVISIBLE);

        new DownloadImageTask(articleViewHolder.image, articleImageManager).execute(imageUrl);

        String commentCount = "";
        int commentCounts = article.getCommentCount();
        if(commentCounts > 0){
            commentCount = String.valueOf(article.getCommentCount() );
        }

        articleViewHolder.commentsCount.setText(commentCount);

        return convertView;
    }

    class ArticleViewHolder {
        TextView title;
        TextView ago;
        TextView score;
        TextView commentsCount;
        LinearLayout scoreWrap;
        LinearLayout commentsWrap;
        ImageView commentsBtn;
        ImageView image;
        LinearLayout contentContainer;
        ImageView shareBtn;
    }

    private void applyReadStyle(TextView textView) {
        textView.setTextColor(getContext().getResources().getColor(R.color.dk_gray));
        textView.setTypeface(textView.getTypeface(), Typeface.NORMAL);
    }

    //-- Interface Methods --//

    public void onTouch(ViewGroup vg, MotionEvent motionEvent, int position) {
        float meRawX = motionEvent.getRawX();
        float meRawY = motionEvent.getRawY();

        Article article = (Article) getItem(position);

        List<View> childViews = SwipeDismissListViewTouchListener.getAllChildred(vg);
        for (View v : childViews) {
            int[] viewCoords = new int[2];
            v.getLocationOnScreen(viewCoords);

            Rect rect = new Rect(0, 0, v.getWidth(), v.getHeight());
            rect.offset(viewCoords[0], viewCoords[1]);

            if (!rect.contains((int) meRawX, (int) meRawY)) {
                continue;
            }

            boolean handled = false;
            switch(v.getId()) {
                case R.id.image:
                    DataApi.markAsRead(article);
                    ViewUtil.getWebViewArticle(this, false, article, context);
                    handled = true;
                    break;
                case R.id.title:
                    DataApi.markAsRead(article);
                    ViewUtil.getWebViewArticle(this, false, article, context);
                    handled = true;
                    break;
                case R.id.commentsWrap:
                    DataApi.markAsRead(article);
                    ViewUtil.getWebViewArticle(this, true, article, context);
                    handled = true;
                    break;
                case R.id.btn_share:
                    ViewUtil.startShareDialog((Activity) context, "");
                    handled = true;
                    break;
                default:
            }

            if (handled) {
                break;
            }
        }
    }
}
